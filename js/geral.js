$(function(){

	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	
	//CARROSSEL DE DESTAQUE
	// $("#carrosselDepoimentos").owlCarousel({
	// 	items : 1,
	// 	dots: true,
	// 	loop: true,
	// 	lazyLoad: true,
	// 	mouseDrag:false,
	// 	touchDrag  : false,
	// 	autoplay:true,
	// 	autoplayTimeout:10000,
	// 	autoplayHoverPause:true,
	// 	smartSpeed: 450,	
	// 	animateOut: 'fadeOut',
	// });
	// var carrosselDepoimentos = $("#carrosselDepoimentos").data('owlCarousel');
	// $('.esquerdaCarrossel').click(function(){ carrosselDepoimentos.prev(); });
	// $('.direitaCarrossel').click(function(){ carrosselDepoimentos.next(); });


	$("#openMenu").click(function() {
	  $("#menuOpen").addClass("menuOpen");
	  $("body").addClass("travScroll");
	});
	$("#closeMenu").click(function() {
	  $("#menuOpen").removeClass("menuOpen");
	  $("body").removeClass("travScroll");
	})

	
	$("#playVideo").click(function() {
	 	$('#video').trigger('play');
	 	$(this).hide();
	});

	$("#video").click(function() {
	 	$('#video').trigger('pause');
	 	$("#playVideo").show();
	});



    $('.pg-prestadorServico section.areaBeneficios .areaSelecao ul li button').click(function(e) {
      
      let dataID = $(this).attr("data-id");

      $('.pg-prestadorServico section.areaBeneficios article').hide();
      $( ".pg-prestadorServico section.areaBeneficios article#" + dataID ).show();

      $('.pg-prestadorServico section.areaBeneficios .areaSelecao ul li button').removeClass( "ativo" );
      $(this).addClass( "ativo" );
      
    });
		
});